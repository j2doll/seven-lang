%% concat_step_3.pl

concatenate( [], List, List ).
concatenate( [ Head | [] ], List, [ Head | List ] ).
concatenate( [ Head1 | [ Head2 | [] ] ], List, [ Head1, Head2 | List ] ).
concatenate( [ Head1 | [ Head2 | [ Head3 | [] ] ] ], List, [ Head1, Head2, Head3 | List ] ).

/*

concatenate( [malfoy, granger], [potter], What ).
What = [malfoy,granger,potter] ? 
yes



*/