%% list-tuple.pl

/* test code */ 

/* 
	list 

(1, 2, 3) = (1, 2, 3).
yes

(1, 2, 3) = (1, 2, 3, 4).
no

(1, 2, 3) = (3, 2, 1).
no

(A, B, C)= (1,2,3).
A = 1
B = 2
C = 3
yes

(A, 2, C)= (1, B, 3).
A = 1
B = 2
C = 3
yes

*/

/*
	tuple 

[1, 2, 3] = [1, 2, 3].
yes

[1, 2, 3] = [3, 2, 1].
no

[X, X, Z] = [2, 2, 3].
X = 2
Z = 3
yes

[X, X, Z] = [2, 4, 3].
no

[] = [].
yes

*/

/*
	tuple uses head and tail.
	tuple uses wildcard(_).

[a, b, c] = [Head | Tail].
Head = a
Tail = [b,c]
yes


[] = [Head | Tail].
no

[a|_] = [Head | Tail].
Head = a
yes

[a] = [Head | Tail].
Head = a
Tail = []
yes

[a,b,c] = [a | Tail].
Tail = [b,c]
yes

[a,b,c] = [a | [Head | Tail] ].
Head = b
Tail = [c]
yes

[a, b, c, d, e] = [ _ , _ |  [ Head | _ ] ].
Head = c
yes

*/





