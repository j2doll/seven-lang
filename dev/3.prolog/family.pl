%% family.pl

/* fact */

father(zeb,         john_boy_sr).
father(john_boy_sr, john_boy_jr).

/* rule */

ancestor(X, Y) :- father(X, Y).
ancestor(X, Y) :- father(X, Z), ancestor(Z, Y). /* recursive clause */

/* query

ancestor(john_boy_sr, john_boy_jr).
true ? a
no

ancestor(zeb, john_boy_jr).
true ?

ancestor(zeb, Who).
Who = john_boy_sr ? a
Who = john_boy_jr
no

ancestor(Who, john_boy_jr).
Who = john_boy_sr ? a
Who = zeb
no

*/
