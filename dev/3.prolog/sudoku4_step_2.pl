%% sudoku4_step_2.pl

sudoku( Puzzle, Solution ) :-
Solution = Puzzle,
Puzzle = 
[S11, S12, S13, S14,
 S21, S22, S23, S24,
 S31, S32, S33, S34,
 S41, S42, S43, S44],
fd_domain( Puzzle, 1, 4 ).

/*

sudoku( [1, 2, 3], Solution ).
no

sudoku( [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4], Solution ).
Solution = [1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4]
yes

sudoku( [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 5], Solution ).
no

*/



