% friends.pl

/* fact, rule, query of prolog */

/* fact */

likes(wallace, cheese).
likes(grommit, cheese).
likes(wendolene, sheep).

/* rule */

friends(X, Y) :- \+(X = Y), likes(X, Z), likes(Y, Z).
% friends means X is not equals to Y, and X likes Z, and Y likes Z.

/* query

	likes(wallace, sheep).
	likes(grommit, cheese).

		The results are as follows.

	no
	yes

		Then, ask a question more.

	friends(wallace, grommit).
	friends(wallace, cheese).
	friends(wallace, wendolene).
	friends(wallace, sheep).
	friends(cheese, sheep).
	friends(grommit, wallace).

*/
