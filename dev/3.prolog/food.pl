% food.pl

/* fact */

food_type(velveeta, cheese).
food_type(ritz, cracker).
food_type(spam, meat).
food_type(sausage, meat).
food_type(jolt, soda).
food_type(twinkie, dessert).

flavor(sweet, dessert).
flavor(savory, meat).
flavor(savory, cheese).
flavor(sweet, soda).

/* rule */

food_flavor(X, Y) :- food_type(X, Z), flavor(Y, Z).

/* query

	food_type(What, meat).
	What = spam ? ;
	What = sausage ? ;	
	no

	flavor(sweet, What).
	What = dessert ? ;
	What = soda
	yes

	food_flavor(What, savory).
	What = velveeta ? ;
	What = spam ? ;
	What = sausage ? ;
	no

*/