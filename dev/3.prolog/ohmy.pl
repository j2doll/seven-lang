# ohmy.pl

/* fact */

cat(lion).
cat(tiger).

/* rule */

dorothy(X, Y, Z) :- X = lion, Y = tiger, Z = bear.
twin_cats(X, Y) :- cat(X), cat(Y).

/* query */
/*
dorothy(lion, tiger, bear).

yes */

/*
dorothy(One, Two, Three).

One = lion
Three = bear
Two = tiger

yes */

/*
twin_cats(One, Two).

One = lion
Two = lion ? a

One = lion
Two = tiger

One = tiger
Two = lion

One = tiger
Two = tiger

yes */
