% map.pl

/* fact */

diffrent(red, green).
diffrent(red, blue).
diffrent(green, red).
diffrent(green, blue).
diffrent(blue, red).
diffrent(blue, green).

/* rule */

coloring(Alabama, Mississippi, Georgia, Tennessee, Florida) :-
 diffrent(Mississippi, Tennessee), 
 diffrent(Mississippi, Alabama), 
 diffrent(Alabama, Tennessee),  
 diffrent(Alabama, Mississippi),   
 diffrent(Alabama, Georgia),   
 diffrent(Alabama, Florida),   
 diffrent(Georgia, Florida),    
 diffrent(Georgia, Tennessee).

/* query

coloring(Alabama, Mississippi, Georgia, Tennessee, Florida).

Alabama = blue
Florida = green
Georgia = red
Mississippi = red
Tennessee = green ? a

Alabama = green
Florida = blue
Georgia = red
Mississippi = red
Tennessee = blue

Alabama = blue
Florida = red
Georgia = green
Mississippi = green
Tennessee = red

Alabama = red
Florida = blue
Georgia = green
Mississippi = green
Tennessee = blue

Alabama = green
Florida = red
Georgia = blue
Mississippi = blue
Tennessee = red

Alabama = red
Florida = green
Georgia = blue
Mississippi = blue
Tennessee = green	

*/
