%% list_math.pl

count( 0, [] ).
count( Count, [Head|Tail] ) :- count( TailCount, Tail ), Count is TailCount+1.

sum( 0, [] ).
sum( Total, [Head|Tail] ) :- sum( Sum, Tail ), Total is Head + Sum.

average( Average, List ) :- sum( Sum, List ), count( Count, List ), Average is Sum/Count.


/*

count( What, [1] ).
What = 1 ?
yes

count( What, [1, b, 100] ).
What = 3 ? 
yes

*/

/*

sum( What, [1, 2, 3] ).
What = 9 ? ;
no

*/

/*

average( What, [10, 20, 30] ).
What = 20.0 ? ;
no

*/