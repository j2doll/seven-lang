//
// operator.io

OperatorTable println
/*
OperatorTable_0xf3df68:
Operators
  0   ? @ @@             zero means priority.
  1   **
  2   % * /
  3   + -
  4   << >>
  5   < <= > >=
  6   != ==
  7   &
  8   ^
  9   |
  10  && and
  11  or ||
  12  ..
  13  %= &= *= += -= /= <<= >>= ^= |=
  14  return

Assign Operators
  ::= newSlot
  :=  setSlot
  =   updateSlot

To add a new operator: OperatorTable addOperator("+", 4) and implement the + message.
To add a new assign operator: OperatorTable addAssignOperator("=", "updateSlot") and implement the updateSlot message.
*/

// define xor(exclusive or) operator
OperatorTable addOperator("xor", 11) // 11 means priority
true xor := method(bool, if(bool, false, true))
false xor := method(bool, if(bool, true, false))

writeln( true xor true ) // false
writeln( true xor false ) // true
writeln( false xor true ) // true
writeln( false xor false ) // false

exit
