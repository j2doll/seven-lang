//
// map.io

elvis := Map clone
elvis atPut( "home", "Graceland" )
writeln( elvis at( "home" ) )

elvis atPut( "style", "rock and roll" )
writeln( elvis asObject )
writeln( elvis asList )
writeln( elvis keys )
writeln( elvis size )

exit
