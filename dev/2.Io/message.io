//
// message.io

// (message)
//   (sender) --[arguments]--> (target)


writeln( postOffice := Object clone )
writeln( postOffice packageSender := method( call sender ) )

writeln( mailer := Object clone )
writeln( mailer deliver := method( postOffice packageSender ) )

writeln( mailer deliver )




exit
