//
// method.io

doFile("prototype.io")
// if you don't want to use doFile(), run the example as follows:
//    io -i prototype.io method.io

writeln("\n--- method.io")

// create a method
writeln(
  method( "So, you've come for an argument." println )
)
writeln( 
	method() type 
) // method is a object. it's 'Block'.

writeln(
  Car drive := method("Vroom" println)
)
ferrai drive

writeln(
	ferrai getSlot("drive")
)

writeln(
	ferrai proto
)

writeln(
	Car proto
)

writeln(
	Lobby
)

exit
