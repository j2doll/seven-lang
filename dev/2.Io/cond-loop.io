//
// cond-loop.io

// loop, while, for

// loop( "getting dizzy ... " println ) // infinite loop

i := 1
while ( i <= 11, i println; i = i + 1 ); "This one goes up to 11" println

for (i, 1, 11, i println); "This one goes up to 11" println
// display from 1 to 11

for (i, 1, 11, 2, i println); "This one goes up to 11" println
// display from 1 to 11 (2 is increment value)

for (i, 1, 2, 1, i println, "extra argument")
// display from 1 to 2 ("extra argument" is ignored.)

for (i, 1, 2, i println, "extra argument")
// display 2 (last value of i is 2)
// "extra argument" is executed

// if

if (true, "It is true." println, "It is false." println)

if (false) then("It is true.") else("It is false.") // nil

if (false) then("It is true." println) else("It is false." println)




exit
