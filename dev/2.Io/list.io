//
// list.io

// list of Io

writeln( toDos := list( "find my car", "find Continuum Transfunctioner" ) )
writeln( toDos size )
writeln( toDos append( "Find a present" ) )

writeln( list(1, 2, 3, 4) average )
writeln( list(1, 2, 3, 4) sum )
writeln( list(1, 2, 3, 4) at(1) )
writeln( list(1, 2, 3) append(4) )
writeln( list(1, 2, 3) pop )
writeln( list(1, 2, 3) prepend(0) )
writeln( list() isEmpty )

exit