// prototype.io
//
// Comments of the //, /**/ and # style are supported

"Hi Ho, Io\n" print
"Yahoo, Io" println
writeln( "Hoyit, Io" ) // I'll use this for sample code. you may use println.

writeln( "\n--- clone prototype" )
writeln( 
	Vehicle := Object clone
)

writeln( "--- set slot of prototype" )
writeln( Vehicle description := "Something to take you places" )

/* error code */
// Vehicle nonexistingSlot = "This won't work"

writeln( "\n--- list slot of prototype" )
writeln( Vehicle slotNames )

writeln( "\n--- type of prototype" )
writeln( Vehicle type )
writeln( Object type )

writeln( "\n--- Car example" )
writeln( "\n------ Car" )

writeln( Car := Vehicle clone )
writeln( Car slotNames )
writeln( Car description ) // Car does not has description. Vehicle has description.

writeln( "\n------ ferrai" )

ferrai := Car clone
// 'type' starts with an uppercase letter.
// 'ferrari' is not 'type'. but it is a object. just like Car.

writeln( ferrai slotNames )
writeln( ferrai type ) // Car

writeln( "\n------ Ferrai" )

writeln( Ferrai := Car clone )
writeln( Ferrai type )

exit
