//
// true-false-nil-singleton.io

writeln( 4 < 5 )
writeln( 4 <= 5 )
writeln( true and false )
writeln( true and true )
writeln( true or true )
writeln( true or false )
writeln( 4 < 5 and 6 > 7 )
writeln( true and 6 )
writeln( true and 0 ) // zero is true.

writeln( "\n --- true" )
writeln(
  true proto
)

// true, false and nil are singleton.
writeln( true clone )
writeln( false clone )
writeln( nil clone )

// make singleton
writeln( Highlander := Object clone )
writeln( Highlander clone := Highlander )

writeln( Highlander clone )

writeln( fred := Highlander clone )
writeln( mike := Highlander clone )
writeln( fred == mike ) // true

// not singleton
writeln( one := Object clone )
writeln( two := Object clone )
writeln( one == two )

// Obect clone := "hosed" // DANGER!!

exit
