=begin
 to_file.rb
 sample of 'mixin'
=end

module ToFile
  def filename
    "object_#{self.object_id}.txt"
  end
  def to_f
    File.open(filename, 'w') { |file| file.write( to_s ) } # to_s() is implemented at class Person
  end
end

class Person
  include ToFile
  attr_accessor :name
  def initialize( name )
    @name = name
  end
  def to_s
    name
  end
end

Person.new('matz').to_f # write a file that contained 'matz'. file name  is 'object_***.txt'.
