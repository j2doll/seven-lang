# collection.rb

# spaceship operator
puts( 'begin' <=> 'end' ) # -1
puts( 'same' <=> 'same' ) # 0

# collection method

a = [ 5, 3, 4, 1 ]

puts( 'a.sort')
puts( a.sort ) # 1 3 4 5

puts( 'a.any? { |i| i > 6 }' )
puts( a.any? { |i| i > 6 } ) # false

puts( 'a.all? { |i| i > 4 }' )
puts( a.all? { |i| i > 4 } ) # false

puts( 'a.collect { |i| i  * 2 }' )
puts( a.collect { |i| i  * 2 } ) # 10 6 8 2

puts( 'a.select { |i| i  % 2 == 0 }' )
puts( a.select { |i| i  % 2 == 1 } ) # 5 3 1

puts( 'a.max' )
puts( a.max ) # 5

puts( 'a.member?(2)' )
puts( a.member?(2) ) # false

# inject
puts( 'a.inject(0) { |sum, i| sum + i }' )
puts( a.inject(0) { |sum, i| sum + i } ) # 13

# each_slice
puts( 'a.each_slice(3) { |a| p a }' )
puts( a.each_slice(3) { |a| p a } ) # [5, 3 ,4] [1]






