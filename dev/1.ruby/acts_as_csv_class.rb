=begin
  acts_as_csv_class.rb
    module
=end

class ActsAsCsv

  def initialize
    @result = []
    read
  end

  def read
    file = File.new( self.class.to_s.downcase + '.txt' )
    @headers = file.gets.chomp.split(',')
    file.each do |row|
      @result << row.chomp.split(',')
    end
  end

  def headers;  @headers; end

  def csv_contents; @result; end

end # class ActsAsCsv

class RubyCsv < ActsAsCsv # superclass is ActsAsCsv
end

m = RubyCsv.new # read rubycsv.txt
puts m.headers.inspect
puts m.csv_contents.inspect
